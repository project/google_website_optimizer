<?php
// Maintained by Nick Schoonens <nick at schoonzie dot com>

/**
 * @file
 * Easily insert the nessesary javascript code for your Google Website Optimizer tests.
 */

/**
 * Administrative settings.
 */
function google_website_optimizer_admin($arg = NULL) {

  $edit = $_POST;
  $op = $_POST['op'];

  $op = $arg && !$op ? $arg : $op;

  switch ($op) {
    case 'add':
      $breadcrumb[] = array('path' => 'admin', 'title' => t('administer'));
      $breadcrumb[] = array('path' => 'admin/settings/google_website_optimizer', 'title' => t('Google Website Optimizer'));
      $breadcrumb[] = array('path' => 'admin/settings/google_website_optimizer/add', 'title' => t('Add new Google Website Optimizer test'));
      drupal_set_breadcrumb($breadcrumb);
      $output = google_website_optimizer_test_form($edit);
      break;

    case 'edit':
      drupal_set_title(t('Edit Google Website Optimizer test'));
      $output = google_website_optimizer_test_form(google_website_optimizer_test_load(urldecode(arg(4))));
      break;

    case 'delete':
      google_website_optimizer_test_delete(urldecode(arg(4)));
      drupal_set_message(t('Deleted test'));
      drupal_goto('admin/settings/google_website_optimizer');
      break;

    case t('Create test');
    case t('Update test');
      if (google_website_optimizer_test_validate($edit)) {
        google_website_optimizer_test_save($edit);
        $edit['old_name'] ? drupal_set_message(t('Your Google Website Optimizer test has been updated.')) : drupal_set_message(t('Your Google Website Optimizer test has been created.'));
        drupal_goto('admin/settings/google_website_optimizer');
      }
      else {
        $output = google_website_optimizer_test_form($edit);
      }
      break;

    default:
      drupal_set_title(t('Google Website Optimizer settings'));
      $output = google_website_optimizer_test_overview();
  }

  return $output;
}
  
/**
 * Remove a test from the database.
 */
function google_website_optimizer_test_delete($tid) {
  db_query("DELETE FROM {google_website_optimizer_tests} WHERE tid = '%d'", $tid);
  db_query("DELETE FROM {google_website_optimizer_tests} WHERE tid = '%d'", $tid);
}

/**
 * Return an HTML form for test configuration.
 */
function google_website_optimizer_test_form_build(&$form_state, $edit) {
  $edit = (object) $edit;

  if (arg(3) == 'add') {
    $btn = t('Create test');
  }
  else {
    $form['tid'] = array('#type' => 'hidden', '#value' => $edit->tid);
    $btn = t('Update test');
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Test name'),
    '#default_value' => $edit->name,
    '#size' => 40,
    '#maxlength' => 128,
    '#description' => t('Enter a name for this test. This should correspond to the name of your test in Google Website Optimizer to make it easy to identify.'),
    '#required' => TRUE
  );

  $form['testing_page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Testing Page'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE
  );

  $form['testing_page']['test_page_path'] = array(
    '#type' => 'textarea',
    '#title' => t('Testing Page(s)'),
    '#default_value' => $edit->test_page_path,
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard.")
  );

  $form['testing_page']['control_script'] = array(
    '#type' => 'textarea',
    '#title' => t('Control Script'),
    '#default_value' => $edit->control_script,
    '#description' => t("Enter the control script provided by Google Website Optimizer")
  );

  $form['testing_page']['tracking_script'] = array(
    '#type' => 'textarea',
    '#title' => t('Tracking Script'),
    '#default_value' => $edit->tracking_script,
    '#description' => t("Enter the tracking script provided by Google Website Optimizer.")
  );

  $form['conversion_page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Conversion Page'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE
  );

  $form['conversion_page']['conversion_page_path'] = array(
    '#type' => 'textarea',
    '#title' => t('Conversion Page(s)'),
    '#default_value' => $edit->conversion_page_path,
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard.")
  );

  $form['conversion_page']['conversion_script'] = array(
    '#type' => 'textarea',
    '#title' => t('Conversion Script'),
    '#default_value' => $edit->conversion_script,
    '#description' => t("Enter the conversion script provided by Google Website Optimizer.")
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => $btn
  );

  return $form;
}

/**
 * Return an HTML form for test configuration.
 */
function google_website_optimizer_test_form($edit) {

  $output .= drupal_get_form('google_website_optimizer_test_form_build', $edit);

  return $output;
}

/**
 * Controller for google_website_optimizer tests.
 */
function google_website_optimizer_test_overview() {
  $output = '';

  $tests = google_website_optimizer_test_load();
  if ($tests) {
    $header = array(t('Test'), t('Operations'));
    foreach ($tests as $t) {
      $rows[] = array(array('data' => $t->name, 'valign' => 'top'), array('data' => l(t('edit'), 'admin/settings/google_website_optimizer/edit/'. urlencode($t->tid)) .' '. l(t('delete'), 'admin/settings/google_website_optimizer/delete/'. urlencode($t->tid)), 'valign' => 'top'));
    }
    $output .= theme('table', $header, $rows);
    $output .= t('<p><a href="!create-test-url">Create new test</a></p>', array('!create-test-url' => url('admin/settings/google_website_optimizer/add')));
  }
  else {
    drupal_set_message(t('No tests found. Click here to <a href="!create-test-url">create a new test</a>.', array('!create-test-url' => url('admin/settings/google_website_optimizer/add'))));
  }

  return $output;
}

/**
 * Save a test to the database.
 */
function google_website_optimizer_test_save($edit) {
  if ($edit['tid']) { //It's an existing test
    db_query("UPDATE {google_website_optimizer_tests} SET name = '%s', test_page_path = '%s', conversion_page_path = '%s', control_script = '%s', tracking_script = '%s', conversion_script = '%s' WHERE tid = '%d'", $edit['name'], $edit['test_page_path'], $edit['conversion_page_path'], $edit['control_script'], $edit['tracking_script'], $edit['conversion_script'], $edit['tid']);
  } 
  else { //It's a new test
    db_query("INSERT INTO {google_website_optimizer_tests} (tid, name, test_page_path, conversion_page_path, control_script, tracking_script, conversion_script) VALUES ('%d', '%s', '%s', '%s', '%s', '%s', '%s')", db_last_insert_id('{google_website_optimizer_tests}', 'tid'), $edit['name'], $edit['test_page_path'], $edit['conversion_page_path'], $edit['control_script'], $edit['tracking_script'], $edit['conversion_script']);
  }
}

/**
 * Test validation.
 */
function google_website_optimizer_test_validate($form, &$form_state) {
  $errors = array();

  if (!$form['name']) {
    $errors['name'] = t('You must give a test name.');
  }

  if (!$form['test_page_path']) {
    $errors['test_page_path'] = t('You must provide the testing page paths.');
  }

  if (!$form['control_script']) {
    $errors['control_script'] = t('You must provide the control script.');
  }

  if (!$form['tracking_script']) {
    $errors['tracking_script'] = t('You must provide the tracking script.');
  }

  if (!$form['conversion_page_path']) {
    $errors['conversion_page_path'] = t('You must provide the conversion page paths.');
  }

  if (!$form['conversion_script']) {
    $errors['conversion_script'] = t('You must provide the conversion script.');
  }

  foreach ($errors as $name => $message) {
    form_set_error($name, $message);
  }

  return count($errors) == 0;
}
